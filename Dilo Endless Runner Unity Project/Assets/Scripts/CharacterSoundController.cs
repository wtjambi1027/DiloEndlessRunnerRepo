﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSoundController : MonoBehaviour
{
    private AudioSource audioPlayer;

    public AudioClip jump;
    public AudioClip scoreHighlight;


    private void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
    }


    public void PlayJump()
    {
        audioPlayer.PlayOneShot(jump);
    }
    

    public void PlayScoreHighlight()
    {
        audioPlayer.PlayOneShot(scoreHighlight);
    }
}
