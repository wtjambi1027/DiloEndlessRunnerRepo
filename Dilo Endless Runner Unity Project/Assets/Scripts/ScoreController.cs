﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [Header("Score Highlight")]
    public int scoreHighlightRange;
    public CharacterSoundController sound;

    private int currentSore = 0;
    private int lastScoreHighlight = 0;

    private void Start()
    {
        // reset
        currentSore = 0;
        lastScoreHighlight = 0;
    }


    public float GetCurrentScore()
    {
        return currentSore;
    }


    public void IncreaseCurrentScore(int increment)
    {
        currentSore += increment;

        if (currentSore - lastScoreHighlight > scoreHighlightRange)
        {
            sound.PlayScoreHighlight();
            lastScoreHighlight += scoreHighlightRange;
            Debug.Log(lastScoreHighlight);
        }
    }


    public void FinishScoring()
    {
        // set high score
        if (currentSore > ScoreData.highScore)
        {
            ScoreData.highScore = currentSore;
        }
    }
}
